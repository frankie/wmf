/*************************************************************
 * WebMailFolder                                             *
 * COPYRIGHT 1998 by Norbert Kuemin <norbert.kuemin@gmx.net> *
 *************************************************************/

#define HTML_FILE
#include "wmf.h"


void InitHtmlCmd() {
    HtmlCmd.DocStart = NULL;
    HtmlCmd.DocEnd = NULL;
    HtmlCmd.HeadStart = NULL;
    HtmlCmd.HeadEnd = NULL;
    HtmlCmd.TitleStart = NULL;
    HtmlCmd.TitleEnd = NULL;
    HtmlCmd.BodyStart = NULL;
    HtmlCmd.BodyEnd = NULL;
    HtmlCmd.RemarkStart = NULL;
    HtmlCmd.RemarkEnd = NULL;
    HtmlCmd.PreStart = NULL;
    HtmlCmd.PreEnd = NULL;
    HtmlCmd.Title1Start = NULL;
    HtmlCmd.Title1End = NULL;
    HtmlCmd.Title2Start = NULL;
    HtmlCmd.Title2End = NULL;
    HtmlCmd.Title3Start = NULL;
    HtmlCmd.Title3End = NULL;
    HtmlCmd.Title4Start = NULL;
    HtmlCmd.Title4End = NULL;
    HtmlCmd.Title5Start = NULL;
    HtmlCmd.Title5End = NULL;
    HtmlCmd.Title6Start = NULL;
    HtmlCmd.Title6End = NULL;
    HtmlCmd.BoldStart = NULL;
    HtmlCmd.BoldEnd = NULL;
    HtmlCmd.ItalicStart = NULL;
    HtmlCmd.ItalicEnd = NULL;
    HtmlCmd.HrStart = NULL;
    HtmlCmd.HrEnd = NULL;
    HtmlCmd.ReplyStart = NULL;
    HtmlCmd.ReplyEnd = NULL;
}


void EndHtmlCmd() {
    if (HtmlCmd.DocStart != NULL) freechar(HtmlCmd.DocStart);
    if (HtmlCmd.DocEnd != NULL) freechar(HtmlCmd.DocEnd);
    if (HtmlCmd.HeadStart != NULL) freechar(HtmlCmd.HeadStart);
    if (HtmlCmd.HeadEnd != NULL) freechar(HtmlCmd.HeadEnd);
    if (HtmlCmd.TitleStart != NULL) freechar(HtmlCmd.TitleStart);
    if (HtmlCmd.TitleEnd != NULL) freechar(HtmlCmd.TitleEnd);
    if (HtmlCmd.BodyStart != NULL) freechar(HtmlCmd.BodyStart);
    if (HtmlCmd.BodyEnd != NULL) freechar(HtmlCmd.BodyEnd);
    if (HtmlCmd.RemarkStart != NULL) freechar(HtmlCmd.RemarkStart);
    if (HtmlCmd.RemarkEnd != NULL) freechar(HtmlCmd.RemarkEnd);
    if (HtmlCmd.PreStart != NULL) freechar(HtmlCmd.PreStart);
    if (HtmlCmd.PreEnd != NULL) freechar(HtmlCmd.PreEnd);
    if (HtmlCmd.Title1Start != NULL) freechar(HtmlCmd.Title1Start);
    if (HtmlCmd.Title1End != NULL) freechar(HtmlCmd.Title1End);
    if (HtmlCmd.Title2Start != NULL) freechar(HtmlCmd.Title2Start);
    if (HtmlCmd.Title2End != NULL) freechar(HtmlCmd.Title2End);
    if (HtmlCmd.Title3Start != NULL) freechar(HtmlCmd.Title3Start);
    if (HtmlCmd.Title3End != NULL) freechar(HtmlCmd.Title3End);
    if (HtmlCmd.Title4Start != NULL) freechar(HtmlCmd.Title4Start);
    if (HtmlCmd.Title4End != NULL) freechar(HtmlCmd.Title4End);
    if (HtmlCmd.Title5Start != NULL) freechar(HtmlCmd.Title5Start);
    if (HtmlCmd.Title5End != NULL) freechar(HtmlCmd.Title5End);
    if (HtmlCmd.Title6Start != NULL) freechar(HtmlCmd.Title6Start);
    if (HtmlCmd.Title6End != NULL) freechar(HtmlCmd.Title6End);
    if (HtmlCmd.BoldStart != NULL) freechar(HtmlCmd.BoldStart);
    if (HtmlCmd.BoldEnd != NULL) freechar(HtmlCmd.BoldEnd);
    if (HtmlCmd.ItalicStart != NULL) freechar(HtmlCmd.ItalicStart);
    if (HtmlCmd.ItalicEnd != NULL) freechar(HtmlCmd.ItalicEnd);
    if (HtmlCmd.HrStart != NULL) freechar(HtmlCmd.HrStart);
    if (HtmlCmd.HrEnd != NULL) freechar(HtmlCmd.HrEnd);
    if (HtmlCmd.ReplyStart != NULL) freechar(HtmlCmd.ReplyStart);
    if (HtmlCmd.ReplyEnd != NULL) freechar(HtmlCmd.ReplyEnd);
}


void InitHtml(char *FileName) {
    
    if (HtmlCmd.DocStart == NULL) {
	NewFile(FileName,HTMLCMD_DOC_START);
    } else {
	NewFile(FileName,HtmlCmd.DocStart);
    }
    AppendFile(FileName,"\n");
}


void EndHtml(char *FileName) {
    
    if (HtmlCmd.DocEnd == NULL) {
	AppendFile(FileName,HTMLCMD_DOC_END);
    } else {
	AppendFile(FileName,HtmlCmd.DocEnd);
    }
    AppendFile(FileName,"\n");
}


void HtmlRemark(char *FileName, char *Text) {
    char *tempstr;
    

    if (HtmlCmd.RemarkStart == NULL) {
	AppendFile(FileName,HTMLCMD_REMARK_START);
    } else {
	AppendFile(FileName,HtmlCmd.RemarkStart);
    }
    if ((FileName == NULL) || (Text == NULL)) return;
    tempstr = newchar(strlen(Text)+11);
    sprintf(tempstr," %s ",Text);
    AppendFile(FileName,tempstr);
    freechar(tempstr);
    if (HtmlCmd.RemarkEnd == NULL) {
	AppendFile(FileName,HTMLCMD_REMARK_END);
    } else {
	AppendFile(FileName,HtmlCmd.RemarkEnd);
    }
    AppendFile(FileName,"\n");
}


void WriteHtmlValue(char *FileName, char *Name, char *Value) {
    char *tempstr;

    tempstr = newchar(strlen(Name)+strlen(Value)+4);
    sprintf(tempstr,"%s=\"%s\"",Name,Value);
    HtmlRemark(FileName,tempstr);
    freechar(tempstr);
}


char *ReadHtmlVarname(char *Line) {
    char *tempstr;
    char *returnstr;
    int length;
    
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
	printf(".ReadHtmlVarname(%s)\n",Line);

    tempstr = Line;

    /* remove spaces */
    while(((tempstr[0] == ' ') || (tempstr[0] == '\t')) && (tempstr[0] != '\0'))
	tempstr++;

    if (HtmlCmd.RemarkStart == NULL)
	if (strncmp(tempstr,HTMLCMD_REMARK_START,strlen(HTMLCMD_REMARK_START)) == 0) {
	    tempstr = tempstr + strlen(HTMLCMD_REMARK_START); /* remove <!-- */
	} else {
	    returnstr = dupchar("\0");
	    return returnstr;
	}
    else
	if (strncmp(tempstr,HtmlCmd.RemarkStart,strlen(HtmlCmd.RemarkStart)) == 0) {
	    tempstr = tempstr + strlen(HtmlCmd.RemarkStart); /* remove <!-- */
	} else {
	    returnstr = dupchar("\0");
	    return returnstr;
	}

    /* remove spaces */
    while(((tempstr[0] == ' ') || (tempstr[0] == '\t')) && (tempstr[0] != '\0'))
	tempstr++;

    length = 0;
    while ((tempstr[length] != '=') && (tempstr[length] != '\0')) length++;
    returnstr = newchar(length+1);
    strncpy(returnstr,tempstr,length);
    returnstr[length] = 0;
    return returnstr;
}


char *ReadHtmlValue(char *Line) {
    char *tempstr;
    char *returnstr;
    int length;
    
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
	printf(".ReadHtmlValue(%s)\n",Line);

    tempstr = Line;
    while ((tempstr[0] != '"') && (tempstr[0] != '\0')) tempstr++; /* Find " */
    if (tempstr[0] == '"') tempstr++; /* remove " */
    length = strlen(tempstr)-1;
    if (length < 0) length = 0;
    while ((tempstr[length] != '"') && (length > 0)) length--;
    returnstr = newchar(length+1);
    if (length > 0) strncpy(returnstr,tempstr,length);
    returnstr[length] = 0;
    return returnstr;
}
