/*************************************************************
 * WebMailFolder                                             *
 * COPYRIGHT 1998 by Norbert Kuemin <norbert.kuemin@gmx.net> *
 *************************************************************/
   
#define SYS_FILE
#include "wmf.h"

#ifndef DMALLOC
char *newchar(int size) {
    char *tmpstr;
    
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 8))
	printf("....newchar(%d)\n",size);
    if (size <= 0) {
	printf("ERROR: trying to allocate %d bytes of memory\n",size);
	size = 1;
    }
    tmpstr = (char *) malloc(size);
    if (tmpstr == NULL) {
	fprintf(stderr,"* not enought memory to allocate another %d bytes *\n",size);
	exit(1);
    }
#ifdef DEBUG
    Debug.StrAlloc++;
#endif
    return tmpstr;
}

char *dupchar(char *string) {
    char *tmpstr;
    
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 8))
	printf("....dupchar(%s)\n",string);
    if (string == NULL) return NULL;
    tmpstr = (char *) strdup(string);
    if (tmpstr == NULL) {
	fprintf(stderr,"* not enought memory to allocate another %d bytes *\n",strlen(string));
	exit(1);
    }
#ifdef DEBUG
    Debug.StrAlloc++;
#endif
    return tmpstr;
}


void freechar(char *string) {

    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 8))
	printf("....freechar(%s)\n",string);
#ifdef DEBUG
    Debug.StrDeAlloc++;
#endif
    if (string == NULL) return;
    free(string);
}
#endif

int isoctal(char number) {
    if ((number >= '0') && (number <= '9')) return TRUE;
    return FALSE;
}
