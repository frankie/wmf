/*************************************************************
 * WebMailFolder                                             *
 * COPYRIGHT 1998 by Norbert Kuemin <norbert.kuemin@gmx.net> *
 *************************************************************/

#define CONFIG_FILE
#include "wmf.h"

/*
   Reading different configurations with the following priority:
   1 = program switches
   2 = giving configuration file (-c)
   3 = enviroments
   4 = defines
*/


void InitConfig() {

    config.ConfigFile = NULL;
    config.Directory = NULL;
    config.MailBoxFile = NULL;
    config.MailCommand = NULL;
    config.Name = NULL;
    config.AboutLink = NULL;
    config.NoFaceFile = NULL;
    config.ReplyChars = NULL;
    config.FaceDirectory = NULL;
    config.DocStartFile = NULL;
    config.DocEndFile = NULL;
    config.DebugLevel = WMF_NOVALUE;
    config.ListLimit = WMF_NOVALUE;
    config.PageSize = WMF_NOVALUE;
    config.SortOrder = WMF_NOVALUE;
    config.WriteMode = WMF_NOVALUE;
    config.BodyHtmlConvert = WMF_NOVALUE;
    config.BodyLinkUrl = WMF_NOVALUE;
    config.BodyLinkEmail = WMF_NOVALUE;
    config.FaceSupport = WMF_NOVALUE;
    config.ThreadNewSubject = WMF_NOVALUE;
    config.NewFilePerm = 0;
    config.ThreadLevelLimit = 0;
    config.AuthorEmailSort = WMF_NOVALUE;
    config.AuthorCaseSort = WMF_NOVALUE;
    config.SubjectCaseSort = WMF_NOVALUE;
    config.ThreadCaseSort = WMF_NOVALUE;
    config.IndexAuthor = WMF_NOVALUE; 
    config.IndexDate = WMF_NOVALUE;
    config.IndexSubject = WMF_NOVALUE;
    config.IndexThread = WMF_NOVALUE;
    config.LineSize = WMF_NOVALUE;
    config.CountMessages = WMF_NOVALUE;
    config.FrameSupport = WMF_NOVALUE;
    config.FrameNameTitle = NULL;
    config.FrameNameIndex = NULL;
    config.FrameNameMail = NULL;
    config.Statistics = WMF_NOVALUE;
    config.Extension = NULL;
}


void DestroyConfig(void) {

    if (config.ConfigFile != NULL) {
	if (config.DebugLevel & 2)
	    printf("..ConfigFile\n");
	freechar(config.ConfigFile);
	config.ConfigFile = NULL;
    }
    if (config.Directory != NULL) {
	if (config.DebugLevel & 2)
	    printf("..Directory\n");
	freechar(config.Directory);
	config.Directory = NULL;
    }
    if (config.MailBoxFile != NULL) {
	if (config.DebugLevel & 2)
	    printf("..MailBoxFile\n");
	freechar(config.MailBoxFile);
	config.MailBoxFile = NULL;
    }
    if (config.MailCommand != NULL) {
	if (config.DebugLevel & 2)
	    printf("..MailCommand\n");
	freechar(config.MailCommand);
	config.MailCommand = NULL;
    }
    if (config.Name != NULL) {
	if (config.DebugLevel & 2)
	    printf("..Name\n");
	freechar(config.Name);
	config.Name = NULL;
    }
    if (config.AboutLink != NULL) {
	if (config.DebugLevel & 2)
	    printf("..AboutLink\n");
	freechar(config.AboutLink);
	config.AboutLink = NULL;
    }
    if (config.NoFaceFile != NULL) {
	if (config.DebugLevel & 2)
	    printf("..NoFaceFile\n");
	freechar(config.NoFaceFile);
	config.NoFaceFile = NULL;
    }
    if (config.ReplyChars != NULL) {
	if (config.DebugLevel & 2)
	    printf("..ReplyChars\n");
	freechar(config.ReplyChars);
	config.ReplyChars = NULL;
    }
    if (config.FaceDirectory != NULL) {
	if (config.DebugLevel & 2)
	    printf("..FaceDirectory\n");
	freechar(config.FaceDirectory);
	config.FaceDirectory = NULL;
    }
    if (config.DocStartFile != NULL) {
	if (config.DebugLevel & 2)
	    printf("..DocStartFile\n");
	freechar(config.DocStartFile);
	config.DocStartFile = NULL;
    }
    if (config.DocEndFile != NULL) {
	if (config.DebugLevel & 2)
	    printf("..DocEndFile\n");
	freechar(config.DocEndFile);
	config.DocEndFile = NULL;
    }
    if (config.FrameNameTitle != NULL) {
	if (config.DebugLevel & 2)
	    printf("..FrameNameTitle\n");
	freechar(config.FrameNameTitle);
	config.FrameNameTitle = NULL;
    }
    if (config.FrameNameIndex != NULL) {
	if (config.DebugLevel & 2)
	    printf("..FrameNameIndex\n");
	freechar(config.FrameNameIndex);
	config.FrameNameIndex = NULL;
    }
    if (config.FrameNameMail != NULL) {
	if (config.DebugLevel & 2)
	    printf("..FrameNameMail\n");
	freechar(config.FrameNameMail);
	config.FrameNameMail = NULL;
    }
    if (config.Extension != NULL) {
	if (config.DebugLevel & 2)
	    printf("..Extension\n");
	freechar(config.Extension);
	config.Extension = NULL;
    }
}


void GetConfigFile () {

/*  1. Configfile from switches
    2. Configfile from enviroment
    3. Configfile from defaults
*/
    
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 1))
        printf(".GetConfigFile\n");
    if ((char *)getenv("WMF_CONFIG") != NULL) {
	if (config.ConfigFile == NULL) {
	    config.ConfigFile = newchar(strlen((char *)getenv("WMF_CONFIG"))+1);
	    strncpy(config.ConfigFile,(char *)getenv("WMF_CONFIG"),strlen((char *)getenv("WMF_CONFIG"))+1);
        }
    }
#ifdef WMF_CONFIG
    if (config.ConfigFile == NULL) {
	config.ConfigFile = dupchar(WMF_CONFIG);
    }
#endif
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
        printf("..configfile = %s\n",(config.ConfigFile ? config.ConfigFile : "(null)"));
}


void ReadDefaults () {

    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 1))
        printf(".ReadDefaults\n");

#ifdef WMF_DEBUGLEVEL
    if (config.DebugLevel == WMF_NOVALUE) {
	config.DebugLevel = WMF_DEBUGLEVEL;
    }
#endif
#ifdef WMF_MBOX
    if (config.MailBoxFile == NULL) {
	config.MailBoxFile = newchar(strlen(WMF_MBOX)+1);
	strncpy(config.MailBoxFile,WMF_MBOX,strlen(WMF_MBOX)+1);
    }
#endif
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
        printf("..mailboxfile = %s\n",config.MailBoxFile);
#ifdef WMF_MBOX
    if (config.MailCommand == NULL) {
	config.MailCommand = newchar(strlen(WMF_MCMD)+1);
	strncpy(config.MailCommand,WMF_MCMD,strlen(WMF_MCMD)+1);
    }
#endif
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
        printf("..mailcommand = %s\n",config.MailCommand);
#ifdef WMF_DIR
    if (config.Directory == NULL) {
	config.Directory = newchar(strlen(WMF_DIR)+1);
	strncpy(config.Directory,WMF_DIR,strlen(WMF_DIR)+1);
    }
#endif
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
        printf("..directory = %s\n",config.Directory);
#ifdef WMF_NAME
    if (config.Name == NULL) {
	config.Name = newchar(strlen(WMF_NAME)+1);
	strncpy(config.Name,WMF_NAME,strlen(WMF_NAME)+1);
    }
#endif
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
        printf("..name = %s\n",(config.Name ? config.Name : "(null)"));
#ifdef HTML_APPEND
    if (config.WriteMode == HTML_NOMODE) {
	config.WriteMode = HTML_APPEND;
    }
#endif
#ifdef HTML_OVERWRITE
    if (config.WriteMode == HTML_NOMODE) {
	config.WriteMode = HTML_OVERWRITE;
    }
#endif
#ifdef WMF_DEFAULT_THREADNEWSUBJECT
    if (config.ThreadNewSubject == HTML_NOMODE) {
	config.ThreadNewSubject = TRUE;
    }
#endif
#ifdef HTML_INDEX
    if (config.WriteMode == HTML_NOMODE) {
	config.WriteMode = HTML_INDEX;
    }
#endif
#ifdef WMF_FRAME_TITLE
    if (config.FrameNameTitle == NULL) {
	config.FrameNameTitle = newchar(strlen(WMF_FRAME_TITLE)+1);
	strncpy(config.FrameNameTitle,WMF_FRAME_TITLE,strlen(WMF_FRAME_TITLE)+1);
    }
#endif
#ifdef WMF_FRAME_INDEX
    if (config.FrameNameIndex == NULL) {
	config.FrameNameIndex = newchar(strlen(WMF_FRAME_INDEX)+1);
	strncpy(config.FrameNameIndex,WMF_FRAME_INDEX,strlen(WMF_FRAME_INDEX)+1);
    }
#endif
#ifdef WMF_FRAME_MAIL
    if (config.FrameNameMail == NULL) {
	config.FrameNameMail = newchar(strlen(WMF_FRAME_MAIL)+1);
	strncpy(config.FrameNameMail,WMF_FRAME_MAIL,strlen(WMF_FRAME_MAIL)+1);
    }
#endif
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
        printf("..writemode = %d\n",config.WriteMode);
    if (config.BodyHtmlConvert == WMF_NOVALUE) config.BodyHtmlConvert = TRUE;
    if (config.BodyLinkUrl == WMF_NOVALUE) config.BodyLinkUrl = TRUE;
    if (config.BodyLinkEmail == WMF_NOVALUE) config.BodyLinkEmail = TRUE;
    if (config.ThreadNewSubject == WMF_NOVALUE) config.ThreadNewSubject = FALSE;
    if (config.AuthorEmailSort == WMF_NOVALUE) config.AuthorEmailSort = FALSE;
    if (config.AuthorCaseSort == WMF_NOVALUE) config.AuthorCaseSort = TRUE;
    if (config.SubjectCaseSort == WMF_NOVALUE) config.SubjectCaseSort = TRUE;
    if (config.ThreadCaseSort == WMF_NOVALUE) config.ThreadCaseSort = TRUE;
    if (config.IndexAuthor == WMF_NOVALUE) config.IndexAuthor = TRUE;
    if (config.IndexDate == WMF_NOVALUE) config.IndexDate = TRUE;
    if (config.IndexSubject == WMF_NOVALUE) config.IndexSubject = TRUE;
    if (config.IndexThread == WMF_NOVALUE) config.IndexThread = TRUE;
    if (config.CountMessages == WMF_NOVALUE) config.CountMessages = TRUE;
    if (config.FrameSupport == WMF_NOVALUE) config.FrameSupport = FALSE;
    if (config.Statistics == WMF_NOVALUE) config.Statistics = FALSE;
#ifdef WMF_EXTENSION
    if (config.Extension == NULL) config.Extension = dupchar(WMF_EXTENSION);
#endif
    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
        printf("..EXTENSION = %s\n",config.Extension);
}


void ReadEnviroments () {

    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 1))
        printf(".ReadEnviroments\n");
    if ((char *)getenv("WMF_DEBUG") != NULL) {
	if (config.DebugLevel == WMF_NOVALUE) {
	    config.DebugLevel = atoi(getenv("WMF_DEBUG"));
	    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
		printf("..WMF_DEBUG = %d\n",config.DebugLevel);
	}
    }
    if ((char *)getenv("WMF_WMODE") != NULL) {
	if (config.WriteMode == WMF_NOVALUE) {
	    config.WriteMode = atoi(getenv("WMF_WMODE"));
	    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
		printf("..WMF_WMODE = %d\n",config.WriteMode);
	}
    }
    if ((char *)getenv("WMF_DAYS") != NULL) {
	if (config.ListLimit == WMF_NOVALUE) {
	    config.ListLimit = time(NULL) - atoi(getenv("WMF_DAYS")) * 24 * 60 * 60;
	    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
		printf("..WMF_DAYS = %s",ctime(&config.ListLimit));
	}
    }
    if ((char *)getenv("WMF_LINES") != NULL) {
	if (config.PageSize == WMF_NOVALUE) {
	    config.PageSize = atoi(getenv("WMF_LINES"));
	    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
		printf("..WMF_LINES = %d\n",config.PageSize);
	}
    }
    if ((char *)getenv("WMF_RSORT") != NULL) {
	if (config.SortOrder == WMF_NOVALUE) {
	    config.SortOrder = SORT_REVERSE;
	    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
		printf("..WMF_SORT = SORT_REVERSE\n");
	}
    }
    
    if ((char *)getenv("WMF_MBOX") != NULL) {
	if (config.MailBoxFile == NULL) {
	    config.MailBoxFile = newchar(strlen((char *)getenv("WMF_MBOX"))+1);
	    strncpy(config.MailBoxFile,(char *)getenv("WMF_MBOX"),strlen((char *)getenv("WMF_MBOX"))+1);
            if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
        	printf("..WMF_MBOX = %s\n",config.MailBoxFile);
        }
    }
    if ((char *)getenv("WMF_DIR") != NULL) {
	if (config.Directory == NULL) {
	    config.Directory = newchar(strlen((char *)getenv("WMF_DIR"))+1);
	    strncpy(config.Directory,(char *)getenv("WMF_DIR"),strlen((char *)getenv("WMF_DIR"))+1);
            if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
        	printf("..WMF_DIR = %s\n",config.Directory);
	}
    }
    if ((char *)getenv("WMF_NAME") != NULL) {
	if (config.Name == NULL) {
	    config.Name = newchar(strlen((char *)getenv("WMF_NAME"))+1);
	    strncpy(config.Name,(char *)getenv("WMF_NAME"),strlen((char *)getenv("WMF_NAME"))+1);
            if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
                printf("..WMF_NAME = %s\n",config.Name);
	}
    }
    if ((char *)getenv("WMF_MCMD") != NULL) {
	if (config.MailCommand == NULL) {
	    config.MailCommand = newchar(strlen((char *)getenv("WMF_MCMD"))+1);
	    strncpy(config.MailCommand,(char *)getenv("WMF_MCMD"),strlen((char *)getenv("WMF_MCMD"))+1);
            if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
        	printf("..WMF_MCMD = %s\n",config.MailCommand);
        }
    }
    if ((char *)getenv("WMF_EXTENSION") != NULL) {
	if (config.Extension == NULL) {
	    config.Extension = newchar(strlen((char *)getenv("WMF_EXTENSION"))+1);
	    strncpy(config.Extension,(char *)getenv("WMF_EXTENSION"),strlen((char *)getenv("WMF_EXTENSION"))+1);
            if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
        	printf("..WMF_EXTENSION = %s\n",config.Extension);
        }
    }
}



void ReadConfigFile (char *FileName) {

    FILE *fd;
    char line[BUFSIZ], *ls_temp, *ls_varname, *ls_varvalue;
    int  li_temp;

    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 1))
        printf(".ReadConfigFile %s\n",FileName);
    fd = fopen(FileName,"r");
    if (fd != NULL) {
	while(fgets(line,BUFSIZ,fd) != NULL) {
	    if (strlen(line) > 0)
		if (line[strlen(line)-1] == '\n')
		    line[strlen(line)-1] = '\0';
	    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
		printf("..Line read = >%s<\n",line);
	    ls_temp = line;

	    /* cut leading spaces */
	    while ((ls_temp[0] == ' ') || (ls_temp[0] == '\t')) ls_temp++;

	    if((ls_temp[0] != '#') && (strchr(ls_temp,'=') != NULL)) {
		/* Get varname */
		li_temp = 0;
		while((ls_temp[li_temp] != '=') && (ls_temp[li_temp] != '\0'))
		    li_temp++;
		if (li_temp > 0) li_temp--;
		while (((ls_temp[li_temp] == ' ') || (ls_temp[li_temp] == '\t')) &&
		    (li_temp > 0)) li_temp--;
		ls_varname = newchar(li_temp+2);
		ls_varname[0] = '\0';
		strncat(ls_varname,ls_temp,li_temp+1);
		for(li_temp=0;li_temp<strlen(ls_varname);li_temp++)
		    ls_varname[li_temp] = toupper(ls_varname[li_temp]);
		
		/* Get varvalue */
		ls_temp = strchr(line,'=');
		if (ls_temp != NULL) ls_temp++;
		while ((ls_temp[0] == ' ') || (ls_temp[0] == '\t')) ls_temp++;
		if (ls_temp[0] == '\"') ls_temp++;
		if (strchr(ls_temp,'\"') != NULL)
		    li_temp = strchr(ls_temp,'\"') - ls_temp;
		else
		    li_temp = strlen(ls_temp);
		ls_varvalue = newchar(li_temp+1);
		ls_varvalue[0] = '\0';
		strncat(ls_varvalue,ls_temp,li_temp);

		if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
		    printf("..Varname = >%s<\n",ls_varname);
		if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 2))
		    printf("..Varvalue = >%s<\n",ls_varvalue);
		if (strcmp(ls_varname,"HTMLBODYSTART") == 0) {
		    if (HtmlCmd.BodyStart != NULL) freechar(HtmlCmd.BodyStart);
		    HtmlCmd.BodyStart = dupchar(ls_varvalue);
		}
		if (strcmp(ls_varname,"HTMLBODYEND") == 0) {
		    if (HtmlCmd.BodyEnd != NULL) freechar(HtmlCmd.BodyEnd);
		    HtmlCmd.BodyEnd = dupchar(ls_varvalue);
		}
		if (strcmp(ls_varname,"HTMLREPLYSTART") == 0) {
		    if (HtmlCmd.ReplyStart != NULL) freechar(HtmlCmd.ReplyStart);
		    HtmlCmd.ReplyStart = dupchar(ls_varvalue);
		}
		if (strcmp(ls_varname,"HTMLREPLYEND") == 0) {
		    if (HtmlCmd.ReplyEnd != NULL) freechar(HtmlCmd.ReplyEnd);
		    HtmlCmd.ReplyEnd = dupchar(ls_varvalue);
		}
		if (strcmp(ls_varname,"HTMLSEPARATOR") == 0) {
		    if (HtmlCmd.HrStart != NULL) freechar(HtmlCmd.HrStart);
		    HtmlCmd.HrStart = dupchar(ls_varvalue);
		}
		if (strcmp(ls_varname,"FACENODATA") == 0) {
		    if (config.NoFaceFile != NULL) freechar(config.NoFaceFile);
		    config.NoFaceFile = dupchar(ls_varvalue);
		}
		if (strcmp(ls_varname,"FACEDIRECTORY") == 0) {
		    if (config.FaceDirectory != NULL) freechar(config.FaceDirectory);
		    config.FaceDirectory = dupchar(ls_varvalue);
		}
		if (strcmp(ls_varname,"ABOUTLINK") == 0) {
		    if (config.AboutLink == NULL) {
			config.AboutLink = dupchar(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"DIRECTORY") == 0) {
		    if (config.Directory == NULL) {
			config.Directory = dupchar(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"MAILCOMMAND") == 0) {
		    if (config.MailCommand == NULL) {
			config.MailCommand = dupchar(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"MAILBOXFILE") == 0) {
		    if (config.MailBoxFile == NULL) {
			config.MailBoxFile = dupchar(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"FOLDERNAME") == 0) {
		    if (config.Name == NULL) {
			config.Name = dupchar(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"DEBUGLEVEL") == 0) {
		    if (config.DebugLevel == WMF_NOVALUE) {
			config.DebugLevel = atoi(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"BODYREPLYCHARS") == 0) {
		    if (config.ReplyChars != NULL) freechar(config.ReplyChars);
		    config.ReplyChars = dupchar(ls_varvalue);
		}
		if (strcmp(ls_varname,"BODYHTMLCONVERT") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.BodyHtmlConvert == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.BodyHtmlConvert = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.BodyHtmlConvert = TRUE;
		    }
		}
		if (strcmp(ls_varname,"BODYLINKURL") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.BodyLinkUrl == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.BodyLinkUrl = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.BodyLinkUrl = TRUE;
		    }
		}
		if (strcmp(ls_varname,"BODYLINKEMAIL") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.BodyLinkEmail == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.BodyLinkEmail = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.BodyLinkEmail = TRUE;
		    }
		}
		if (strcmp(ls_varname,"FACESUPPORT") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.FaceSupport == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.FaceSupport = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.FaceSupport = TRUE;
		    }
		}
		if (strcmp(ls_varname,"NEWSUBJECTTHREAD") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.ThreadNewSubject == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.ThreadNewSubject = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.ThreadNewSubject = TRUE;
		    }
		}
		if (strcmp(ls_varname,"AUTHOREMAILSORT") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.AuthorEmailSort == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.AuthorEmailSort = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.AuthorEmailSort = TRUE;
		    }
		}
		if (strcmp(ls_varname,"AUTHORCASESORT") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.AuthorCaseSort == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.AuthorCaseSort = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.AuthorCaseSort = TRUE;
		    }
		}
		if (strcmp(ls_varname,"SUBJECTCASESORT") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.SubjectCaseSort == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.SubjectCaseSort = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.SubjectCaseSort = TRUE;
		    }
		}
		if (strcmp(ls_varname,"THREADCASESORT") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.ThreadCaseSort == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.ThreadCaseSort = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.ThreadCaseSort = TRUE;
		    }
		}
		if (strcmp(ls_varname,"INDEXAUTHOR") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.IndexAuthor == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.IndexAuthor = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.IndexAuthor = TRUE;
		    }
		}
		if (strcmp(ls_varname,"INDEXDATE") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.IndexDate == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.IndexDate = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.IndexDate = TRUE;
		    }
		}
		if (strcmp(ls_varname,"INDEXSUBJECT") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.IndexSubject == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.IndexSubject = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.IndexSubject = TRUE;
		    }
		}
		if (strcmp(ls_varname,"INDEXTHREAD") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.IndexThread == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.IndexThread = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.IndexThread = TRUE;
		    }
		}
		if (strcmp(ls_varname,"DAYLIMIT") == 0) {
		    if (config.ListLimit == WMF_NOVALUE) {
			config.ListLimit = time(NULL) - atoi(ls_varvalue) * 24 * 60 * 60;
		    }
		}
		if (strcmp(ls_varname,"FILEPERMISSION") == 0) {
		    if (config.NewFilePerm == 0) {
			for(li_temp=0;li_temp<(strlen(ls_varvalue));li_temp++) {
			    config.NewFilePerm = config.NewFilePerm*8+ls_varvalue[li_temp]-48;
			}
		    }
		}
		if (strcmp(ls_varname,"PAGESIZE") == 0) {
		    if (config.PageSize == WMF_NOVALUE) {
			config.PageSize = atoi(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"SORTORDER") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.SortOrder == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"NEWFIRST") == 0) 
			    config.SortOrder = SORT_REVERSE;
			if (strcmp(ls_varvalue,"NEWLAST") == 0) 
			    config.SortOrder = SORT_REVERSE;
		    }
		}
		if (strcmp(ls_varname,"WRITEMODE") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.WriteMode == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"INDEXONLY") == 0) 
			    config.WriteMode = HTML_INDEX;
			if (strcmp(ls_varvalue,"OVERWRITE") == 0) 
			    config.WriteMode = HTML_OVERWRITE;
			if (strcmp(ls_varvalue,"APPEND") == 0) 
			    config.WriteMode = HTML_APPEND;
			if (strcmp(ls_varvalue,"APPENDNOINDEX") == 0) 
			    config.WriteMode = HTML_APPENDNOINDEX;
		    }
		}
		if (strcmp(ls_varname,"DOCSTARTFILE") == 0) {
		    if (config.DocStartFile == NULL) {
			config.DocStartFile = dupchar(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"DOCENDFILE") == 0) {
		    if (config.DocEndFile == NULL) {
			config.DocEndFile = dupchar(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"LINESIZE") == 0) {
		    if (config.LineSize == WMF_NOVALUE) {
			config.LineSize = atoi(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"MESSAGECOUNT") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.CountMessages == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.CountMessages = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.CountMessages = TRUE;
		    }
		}
		if (strcmp(ls_varname,"FRAMESUPPORT") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.FrameSupport == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.FrameSupport = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.FrameSupport = TRUE;
		    }
		}
		if (strcmp(ls_varname,"FRAMENAMETITLE") == 0) {
		    if (config.FrameNameTitle == NULL) {
			config.FrameNameTitle = dupchar(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"FRAMENAMEINDEX") == 0) {
		    if (config.FrameNameIndex == NULL) {
			config.FrameNameIndex = dupchar(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"FRAMENAMEMAIL") == 0) {
		    if (config.FrameNameMail == NULL) {
			config.FrameNameMail = dupchar(ls_varvalue);
		    }
		}
		if (strcmp(ls_varname,"STATISTICS") == 0) {
		    for(li_temp=0;li_temp<strlen(ls_varvalue);li_temp++)
			ls_varvalue[li_temp] = toupper(ls_varvalue[li_temp]);
		    if (config.Statistics == WMF_NOVALUE) {
			if (strcmp(ls_varvalue,"OFF") == 0)
			    config.Statistics = FALSE;
			if (strcmp(ls_varvalue,"ON") == 0)
			    config.Statistics = TRUE;
		    }
		}
		if (strcmp(ls_varname,"EXTENSION") == 0) {
		    if (config.Extension == NULL) {
			config.Extension = dupchar(ls_varvalue);
		    }
		}
		/* Cleanup */
		freechar(ls_varname);
		freechar(ls_varvalue);
	    }
	}
	fclose(fd);
    } else {
#ifdef WMF_CONFIG
	if (strcmp(FileName,CheckHomeDir(WMF_CONFIG)) != 0)
#endif
	fprintf(stderr,"warning: unable to read configfile \"%s\"\n",FileName);
    }
}



void ReadConfig () {

    char *tmpstr;

    if ((config.DebugLevel != WMF_NOVALUE) && (config.DebugLevel & 1))
        printf(".ReadConfig\n");

    InitHtmlCmd();
    GetConfigFile();
    tmpstr = config.ConfigFile;
    config.ConfigFile = CheckHomeDir(tmpstr);
    if (tmpstr != NULL) freechar(tmpstr);
    if (config.ConfigFile != NULL) ReadConfigFile(config.ConfigFile);
    ReadEnviroments();
    ReadDefaults();
    tmpstr = config.MailBoxFile;
    config.MailBoxFile = CheckHomeDir(tmpstr);
    if (tmpstr != NULL) freechar(tmpstr);
    tmpstr = config.Directory;
    config.Directory = CheckHomeDir(tmpstr);
    if (tmpstr != NULL) freechar(tmpstr);
}
