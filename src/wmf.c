/*************************************************************
 * WebMailFolder                                             *
 * COPYRIGHT 1998 by Norbert Kuemin <norbert.kuemin@gmx.net> *
 *************************************************************/
   
#define MAIN_FILE
#include "wmf.h"

void ShowUsage(int level) {
    printf("  usage: %s [option]\n\n",config.ProgramName);
    if (level>0) {
	printf("    -?        show this help text\n");
	printf("    -a url    about hyperlink\n");
	printf("    -c file   specify configuration file\n");
	printf("    -d dir    directory\n");
	printf("    -e cmd    mail command\n");
	printf("    -f mode   same as umask for creating new files\n");
	printf("    -h        show this help text\n");
	printf("    -l days   day limit\n");
	printf("    -m mbox   mbox file (- for stdin)\n");
	printf("    -n name   name of the mailinglist\n");
	printf("    -p x      lines / page index\n");
	printf("    -r        reverse sorting (Newest first)\n");
	printf("    -t level  threading level limitation\n");
	printf("    -v        print the programname and the version\n");
	printf("    -w x      write-mode (0=index 1=overwrite 2=append 3=append+noindex)\n");
	printf("    -x x      debug level\n");
    }
}

void Cleanup() {

    /* Deallocate memory */
    if (config.DebugLevel & 1)
        printf(".cleanup\n");

    /* Free LastFileName and close open files */ 
    AppendFile("","");

    /* Throw Html commands away */
    EndHtmlCmd();

    /* Destroy configuration */
    DestroyConfig();
}


int main (int argc, char **argv) {

    extern char *optarg;
    char opt;
    int i;
            
    /* Initial */
    config.ProgramName = argv[0];
    InitConfig();
#ifdef DEBUG
    Debug.StrAlloc = 0;
    Debug.StrDeAlloc = 0;
#endif

    while ((opt = getopt(argc, argv, "?hvra:c:d:e:f:l:m:n:p:t:w:x:")) != '\xFF') {
        switch (opt) {
	    case '?': /* help information */
	    case 'h':
	        ShowUsage(1);
	        exit(0);
	        break;
	    case 'v': /* show current version */
	        printf("%s version %s\n",NAME,VERSION);
	        exit(0);
	        break;
	    case 'w': /* write-mode */
		config.WriteMode = atoi(optarg);
	        break;
	    case 't': /* thread level limit */
		config.ThreadLevelLimit = atoi(optarg);
	        break;
	    case 'l': /* list limit */
		config.ListLimit = time(NULL) - atoi(optarg) * 24 * 60 * 60;
	        break;
	    case 'p': /* page size */
		config.PageSize = atoi(optarg);
	        break;
	    case 'f': /* permissions for new files */
		for(i=0;i<(strlen(optarg));i++) {
		    config.NewFilePerm = config.NewFilePerm*8+optarg[i]-48;
		}
	        break;
	    case 'x': /* debug level */
		config.DebugLevel = atoi(optarg);
	        break;
	    case 'r': /* sort reverse */
		config.SortOrder = SORT_REVERSE;
	        break;
	    case 'c': /* another config-file */
                if (config.ConfigFile != NULL) {
                    freechar(config.ConfigFile);
                }
                config.ConfigFile = newchar(strlen(optarg)+1);
                strncpy(config.ConfigFile,optarg,strlen(optarg)+1);
	        break;
	    case 'd': /* directory */
                if (config.Directory != NULL) {
                    freechar(config.Directory);
                }
                config.Directory = newchar(strlen(optarg)+1);
                strncpy(config.Directory,optarg,strlen(optarg)+1);
	        break;
	    case 'n': /* name */
                if (config.Name != NULL) {
                    freechar(config.Name);
                }
                config.Name = newchar(strlen(optarg)+1);
                strncpy(config.Name,optarg,strlen(optarg)+1);
	        break;
	    case 'm': /* mailbox file */
                if (config.MailBoxFile != NULL) {
                    freechar(config.MailBoxFile);
                }
                config.MailBoxFile = newchar(strlen(optarg)+1);
                strncpy(config.MailBoxFile,optarg,strlen(optarg)+1);
	        break;
	    case 'e': /* mail command */
                if (config.MailCommand != NULL) {
                    freechar(config.MailCommand);
                }
                config.MailCommand = newchar(strlen(optarg)+1);
                strncpy(config.MailCommand,optarg,strlen(optarg)+1);
	        break;
	    case 'a': /* about link url */
                if (config.AboutLink != NULL) {
                    freechar(config.AboutLink);
                }
                config.AboutLink = dupchar(optarg);
	        break;
	    default:
		ShowUsage(0);
	        exit(1);
	        break;
        }
    }
    
    ReadConfig();
    if (config.Directory != NULL) {
	if (config.Directory[strlen(config.Directory)-1] == '/')
	    config.Directory[strlen(config.Directory)-1] = '\0';
	if (!FileExist(config.Directory)) {
	    fprintf(stderr,"Error: directory %s doesn't exist or isn't readable.\n",config.Directory);
	    exit(1);
	}
    }
    if (config.MailBoxFile != NULL) Mail2Html(config.MailBoxFile);
    if (config.WriteMode != HTML_APPENDNOINDEX) CreateIndex();

    Cleanup();

#ifdef DEBUG
    printf("Allocated strings:%d\n",Debug.StrAlloc);
    printf("Deallocated strings:%d\n",Debug.StrDeAlloc);
#endif
    return 0;
}
